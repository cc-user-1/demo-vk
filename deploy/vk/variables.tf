# Provider settings

variable "vkcs_username" {
  type = string
}

variable "vkcs_password" {
  type = string
}

variable "vkcs_project_id" {
  type = string
}

variable "vkcs_region" {
  type = string
}

# App settings

variable "name" {
  type        = string
  default     = "demovk"
  description = "Application name"
}

variable "image_name" {
  type    = string
}

variable "server_port" {
  type    = number
  default = 8080
}

variable "env" {
  type    = map(string)
  default = {}
}

# Compute

variable "vm_flavor" {
  type = string
  default = "Basic-1-2-20"
}

variable "vm_az" {
  type = string
  default = "GZ1"
}

variable "network_cidr" {
  type = string
  default = "10.0.0.0/24"
}

variable "create_floating_ip" {
  type = bool
  default = true
}

# DB

variable "main_db_flavor" {
  type = string
  default = "Standard-2-8-50"
}

variable "main_db_name" {
  type    = string
  default = "demovk"
}

variable "main_db_engine" {
  type    = string
  default = "postgresql"
}

variable "main_db_engine_version" {
  type    = string
  default = "14"
}

variable "main_db_user" {
  type    = string
  default = "demovk"
}

variable "main_db_password" {
  type  = string
  default = ""
  sensitive = true
}

variable "main_db_random_password" {
  type    = bool
  default = true
}
